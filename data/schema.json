{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "FM Tools Data  Schema",
  "description": "The schema for competition tools",
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "input_languages": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "project_url": {
      "type": "string",
      "format": "uri"
    },
    "repository_url": {
      "type": "string",
      "format": "uri"
    },
    "spdx_license_identifier": {
      "type": "string"
    },
    "coveriteam_actor": {
      "type": "string"
    },
    "benchexec_toolinfo_module": {
      "type": "string",
      "format": "uri"
    },
    "fmtools_format_version": {
      "type": "string"
    },
    "fmtools_entry_maintainers": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "maintainers": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "institution": {
            "type": "string"
          },
          "country": {
            "type": "string"
          },
          "url": {
            "type": [
              "string",
              "null"
            ],
            "format": "uri"
          }
        },
        "required": [
          "name",
          "institution",
          "country",
          "url"
        ]
      }
    },
    "versions": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "version": {
            "type": "string"
          },
          "doi": {
            "type": "string",
            "pattern": "10\\.5281\/zenodo\\.[0-9]+$"
          },
          "url": {
            "type": [
              "string",
              "null"
            ],
            "format": "uri"
          },
          "benchexec_toolinfo_options": {
            "type": "array",
            "items": {
              "type": "string"
            }
          },
          "required_ubuntu_packages": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        },
        "oneOf": [
          {
            "required": [
              "version",
              "benchexec_toolinfo_options",
              "required_ubuntu_packages",
              "doi"
            ]
          },
          {
            "required": [
              "version",
              "benchexec_toolinfo_options",
              "required_ubuntu_packages",
              "url"
            ]
          }
        ]
      }
    },
    "competition_participations": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "competition": {
            "type": "string",
            "pattern": "(SV-COMP|Test-Comp) 20[1-9][0-9]$"
          },
          "track": {
            "type": "string",
            "enum": [
              "Validation of Correctness Witnesses",
              "Validation of Violation Witnesses",
              "Verification",
              "Test Generation",
              "Validation of Test Suites"
            ]
          },
          "tool_version": {
            "type": "string"
          },
          "jury_member": {
            "type": "object",
            "properties": {
              "name": {
                "type": "string"
              },
              "institution": {
                "type": "string"
              },
              "country": {
                "type": "string"
              },
              "url": {
                "type": [
                  "string",
                  "null"
                ]
              }
            },
            "required": [
              "name",
              "institution",
              "country",
              "url"
            ]
          }
        },
        "required": [
          "competition",
          "track",
          "tool_version",
          "jury_member"
        ]
      }
    },
    "techniques": {
      "type": "array",
      "items": {
        "type": "string",
        "enum": [
          "CEGAR",
          "Predicate Abstraction",
          "Symbolic Execution",
          "Bounded Model Checking",
          "k-Induction",
          "Property-Directed Reachability",
          "Explicit-Value Analysis",
          "Numeric Interval Analysis",
          "Shape Analysis",
          "Separation Logic",
          "Bit-Precise Analysis",
          "ARG-Based Analysis",
          "Lazy Abstraction",
          "Interpolation",
          "Automata-Based Analysis",
          "Concurrency Support",
          "Ranking Functions",
          "Evolutionary Algorithms",
          "Algorithm Selection",
          "Portfolio",
          "Interpolation-Based Model Checking",
          "Logic Synthesis",
          "Task Translation",
          "Floating-Point Arithmetics",
          "Guidance by Coverage Measures",	
          "Random Execution",	
          "Targeted Input Generation"
        ]
      }
    },
    "literature": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "doi": {
            "type": "string",
            "pattern": "10\\.[0-9]{4,9}\\/.*"
          },
          "title": {
            "type": "string"
          },
          "year": {
            "type": "integer"
          }
        },
        "required": [
          "doi",
          "title",
          "year"
        ]
      }
    }
  },
  "required": [
    "name",
    "input_languages",
    "project_url",
    "spdx_license_identifier",
    "coveriteam_actor",
    "benchexec_toolinfo_module",
    "fmtools_format_version",
    "fmtools_entry_maintainers",
    "maintainers",
    "versions",
    "competition_participations",
    "techniques"
  ]
}